/*
  Warnings:

  - You are about to drop the `User` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "User";

-- CreateTable
CREATE TABLE "myData" (
    "id" TEXT NOT NULL,
    "annee_publication" TEXT NOT NULL,
    "code_departement" TEXT NOT NULL,
    "nom_departement" TEXT NOT NULL,
    "code_region" TEXT NOT NULL,
    "nom_region" TEXT NOT NULL,
    "nombre_d_habitants" TEXT NOT NULL,
    "densite_de_population_au_km2" TEXT NOT NULL,
    "variation_de_la_population_sur_10_ans_en" TEXT NOT NULL,
    "dont_contribution_du_solde_naturel_en" TEXT NOT NULL,
    "dont_contribution_du_solde_migratoire_en" TEXT NOT NULL,
    "population_de_moins_de_20_ans" TEXT NOT NULL,
    "population_de_60_ans_et_plus" TEXT NOT NULL,
    "taux_de_chomage_au_t4_en" TEXT NOT NULL,
    "taux_de_pauvrete_en" TEXT NOT NULL,
    "nombre_de_logements" TEXT NOT NULL,
    "nombre_de_residences_principales" TEXT NOT NULL,
    "taux_de_logements_sociaux_en" TEXT NOT NULL,
    "taux_de_logements_vacants_en" TEXT NOT NULL,
    "taux_de_logements_individuels_en" TEXT NOT NULL,
    "moyenne_annuelle_de_la_construction_neuve_sur_10_ans" TEXT NOT NULL,
    "construction" TEXT NOT NULL,
    "parc_social_nombre_de_logements" TEXT NOT NULL,
    "parc_social_logements_mis_en_location" TEXT NOT NULL,
    "parc_social_logements_demolis" TEXT NOT NULL,
    "parc_social_ventes_a_des_personnes_physiques" TEXT NOT NULL,
    "parc_social_taux_de_logements_vacants_en" TEXT NOT NULL,
    "parc_social_taux_de_logements_individuels_en" TEXT NOT NULL,
    "parc_social_loyer_moyen_en_eur_m2_mois" TEXT NOT NULL,
    "parc_social_age_moyen_du_parc_en_annees" TEXT NOT NULL,
    "parc_social_taux_de_logements_energivores_e_f_g_en" TEXT NOT NULL,

    CONSTRAINT "myData_pkey" PRIMARY KEY ("id")
);
