import Fastify from 'fastify';
import cors from '@fastify/cors';
import registerRoutes from './routes/index';
import multer from 'fastify-multer';
import fasifyStatic from '@fastify/static';
import path from 'path';

const fastify = Fastify({
	logger: {
		transport: {
			target: 'pino-pretty',
		},
	},
});

fastify.register(cors, {
	origin: '*',
});

fastify.register(multer.contentParser);
fastify.register(registerRoutes);

fastify.register(fasifyStatic, {
	root: path.join(__dirname, 'uploads'),
	prefix: '/public/',
});

try {
	fastify.listen({ port: 3000, host: '0.0.0.0' });
} catch (err) {
	fastify.log.error(err);
	process.exit(1);
}
