import multer from 'fastify-multer';
import path from 'path';
import { PUBLIC_PATH } from '../config';
const limits = { fileSize: 1024 * 1024 * 24 }; // 24 MB

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, PUBLIC_PATH);
	},
	filename: function (req, file, cb) {
		console.log('PUBLIC_PATH', PUBLIC_PATH);

		const fileExt = path.extname(file.originalname);
		const filename = file.fieldname + '-' + Date.now() + fileExt;
		cb(null, filename);
	},
});

const multerUpload = multer({
	storage,
	limits,
});

export default multerUpload;
