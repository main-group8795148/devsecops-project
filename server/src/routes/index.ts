import Controller from '../controllers/Controller';

const routes = async (app, options) => {
	app.get('/', (req, res) => res.send('Hello World!'));
	app.get('/data', Controller.findMany);
};

export default routes;
