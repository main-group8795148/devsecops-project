# Consignes

<strong>Développer une application de statistique du logement par département :</strong>

-   [x] Proposer une interface de recherche d'info selon 5 critères au choix
-   [x] Présenter des graphiques de statistiques :
    -   [x] nbre habitants/region
    -   [x] taux_chômage/region
    -   [x] nombre_construction/region
-   [x] Livrer une version de l'application sous forme d'image docker dans la registry gitlab
-   [ ] Analyser les vulnérabilités de l'image docker
-   [ ] Construire une chaine CI/CD avec du SCA, SAST et DAST
-   [ ] Présenter les rapports de sécurité en proposant des solutions à 3 vulnérabilités SCA, SAST et DAST

<br>

# Build Dockerfile server

```
docker build -t my-server .
docker run -p 3000:3000 --name my-server my-server
```

# Build Dockerfile CLient

```
docker build -t my-client .
docker run -p 5173:5173 --name my-client my-client
```
