import { Toast } from 'flowbite-react';
import { useEffect, useState } from 'react';
import { HiCheck, HiExclamation, HiX } from 'react-icons/hi';

type JToastProps = {
	type: 'success' | 'error' | 'warning';
	message: string;
	display: boolean;
};

const JToast = ({ type, message, display }: JToastProps) => {
	const themes = {
		success: {
			icon: <HiCheck className='h-5 w-5' />,
			style: 'bg-green-100 text-green-500 dark:bg-green-800 dark:text-green-200',
		},
		error: {
			icon: <HiX className='h-5 w-5' />,
			style: 'bg-red-100 text-red-500 dark:bg-red-800 dark:text-red-200',
		},
		warning: {
			icon: <HiExclamation className='h-5 w-5' />,
			style: 'bg-orange-100 text-orange-500 dark:bg-orange-700 dark:text-orange-200',
		},
	} as const;

	const [show, setShow] = useState(display);

	useEffect(() => {
		setShow(display);

		setTimeout(() => {
			setShow(false);
		}, 5000);
	}, [display]);

	return (
		<>
			{show && (
				<Toast className={`absolute bottom-12 right-2`}>
					<div
						className={`${themes[type]?.style} inline-flex h-8 w-8 shrink-0 items-center justify-center rounded-lg `}
					>
						<HiCheck className='h-5 w-5' />
					</div>
					<div className='ml-3 text-sm font-normal'>{message}.</div>
				</Toast>
			)}
		</>
	);
};

export default JToast;
